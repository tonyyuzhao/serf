FROM golang:1.11.2-alpine3.8
MAINTAINER Tony Zhao (tony.yu.zhao@gmail.com)

COPY ./binaries/serf_0.8.1_linux_amd64.zip .
COPY ./cmd/run.sh .

#EXPOSE

RUN apk update && \
    apk upgrade && \
    unzip serf_0.8.1_linux_amd64.zip -d /usr/local/bin && \
    chmod +x /usr/local/bin/serf && \
    chmod +x run.sh

ENTRYPOINT ["./run.sh"]

