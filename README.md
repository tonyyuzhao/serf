```
docker network create serf
docker run --net=serf -d -p 7373:7373/tcp serf <name> <bind-ip> # master
docker run --net=serf -d serf <name> <bind-ip> # slaves
```
To connect:

```
docker exec -i -t <container> /bin/sh
```

To join within a container:
```
serf join -rpc-addr=<ip>:7373 <cluster>
serf members -rpc-addr=<ip>:7373
```
