package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/hashicorp/serf/client"
	"github.com/hashicorp/serf/coordinate"
	"log"
	"math"
	"net/http"
)

type Node struct {
	Id    string `json:"id"`
	Addr  string `json:"addr"`
	Group int    `json:"group"`
}

type Link struct {
	Source string  `json:"source"`
	Target string  `json:"target"`
	Length float64 `json:"length"`
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/members.json", members).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", router))
}

func members(writer http.ResponseWriter, request *http.Request) {
	serf, err := client.NewRPCClient("127.0.0.1:7373")
	if err != nil {
		fmt.Printf("error creating rpc client %v\n", err)
		_ = json.NewEncoder(writer).Encode(struct {
			Error string `json:"error"`
		}{
			err.Error()})
		return
	}
	defer serf.Close()

	writer.Header().Set("Content-Type", "application/json")
	writer.Header().Set("Access-Control-Allow-Origin", "*")

	members, _ := serf.Members()

	nodes := make([]Node, 0)
	links := make([]Link, 0)

	for i, n := range members {
		nodes = append(nodes, Node{Id: n.Name, Addr: n.Addr.String(), Group: i})
		for _, m := range members[i+1:] {
			nc, _ := serf.GetCoordinate(n.Name)
			mc, _ := serf.GetCoordinate(m.Name)
			links = append(links, Link{Source: n.Name, Target: m.Name, Length: distance(nc, mc)})
		}
	}

	_ = json.NewEncoder(writer).Encode(struct {
		Nodes []Node `json:"nodes"`
		Links []Link `json:"links"`
	}{nodes, links})
}

func distance(a *coordinate.Coordinate, b *coordinate.Coordinate) float64 {
	if len(a.Vec) != len(b.Vec) {
		panic("dimensions aren't compatible")
	}

	// Calculate the Euclidean distance plus the heights.
	sumsq := 0.0
	for i := 0; i < len(a.Vec); i++ {
		diff := a.Vec[i] - b.Vec[i]
		sumsq += diff * diff
	}
	rtt := math.Sqrt(sumsq) + a.Height + b.Height

	// Apply the adjustment components, guarding against negatives.
	adjusted := rtt + a.Adjustment + b.Adjustment
	if adjusted > 0.0 {
		rtt = adjusted
	}

	return rtt * 5e05
}
